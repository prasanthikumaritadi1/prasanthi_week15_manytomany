package com.gl.repsitory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.entity.Publisher;

public interface PublisherRepository extends JpaRepository<Publisher, Integer> {

}
