package com.gl;

import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gl.entity.Book;
import com.gl.entity.BookPublisher;
import com.gl.entity.Publisher;
import com.gl.repsitory.BookRepository;
import com.gl.repsitory.PublisherRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@SpringBootApplication
public class Week15GlMtoMApplication implements CommandLineRunner {

	@Autowired
	private BookRepository bookRepository;
	@Autowired
    private PublisherRepository publisherRepository;
    
	public static void main(String[] args) {
		SpringApplication.run(Week15GlMtoMApplication.class, args);
	}
	
	 @Override
	    public void run(String... args) {
	        // Create a couple of Book, Publisher and BookPublisher
	        Publisher publisherA = new Publisher("Publisher A");
	        Publisher publisherB = new Publisher("Publisher B");
	        publisherRepository.saveAll(Arrays.asList(publisherA, publisherB));

	        bookRepository.save(new Book("Book 1", new BookPublisher(publisherA, new Date()), new BookPublisher(publisherB, new Date())));
	        bookRepository.save(new Book("Book 2", new BookPublisher(publisherA, new Date())));
	 }
}

